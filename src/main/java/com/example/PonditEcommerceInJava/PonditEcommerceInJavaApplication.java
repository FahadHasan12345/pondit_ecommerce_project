package com.example.PonditEcommerceInJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PonditEcommerceInJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PonditEcommerceInJavaApplication.class, args);
	}

}
