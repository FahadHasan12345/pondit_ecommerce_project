package com.example.PonditEcommerceInJava.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

/**
 * The persistent class for the ecommerce_product database table.
 *
 * @author Fahad Hasan
 * 
 * @since 2020-11-30
 */

@Data
@Entity
@Table(name = "ecommerce_product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(value = AccessLevel.PRIVATE)
	@Column(updatable = false, nullable = false)
	private Long id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String color;
	
	@NotNull
	private String image;
	
	@NotNull
	private int size;
	
	@NotNull
	private int price;
	
	@Column(nullable = false)
	@NonNull
	private LocalDate creationDate = LocalDate.now();
	// Add Category
	// Ad Review
	//Add image
}
